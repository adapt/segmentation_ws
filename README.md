Setup:  
git clone --recurse-submodules  
  
Download the following weights into hrnet/pretrained_models:  
https://onedrive.live.com/?authkey=%21AAvg66s5bRFfgkc&cid=F7FD0B7F26543CEB&id=F7FD0B7F26543CEB%21160&parId=F7FD0B7F26543CEB%21118&o=OneUp  
  
Running:  
python3 hrnet_demo.py  

HRNet is a fork of:  
https://github.com/HRNet/HRNet-Semantic-Segmentation  


cat_color_map = {
                    1 : (255,255,255),    # ice
                    2 : (0, 0, 255),      # water
                    3 : (200,200,200),    # ice-water-mix
                    4 : (42, 150, 42),    # land
                    5 : (200, 200, 255)   # snow-on-land
}

# License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
