import sys
import time
import cv2
import numpy as np
import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
import matplotlib.pyplot as plt
from thop import profile, clever_format
from torch.nn import functional as F
from PIL import Image

# HRnet
sys.path.insert(0, 'hrnet/lib/')
import models
import datasets
from config.default import _C as cfg

# Cityscapes
sys.path.insert(0, 'cityscapesScripts')
from cityscapesscripts.helpers.labels import labels

# Convert master cityscapes label map into convenient dict {label_id : class_name}
label_map = {label.trainId : label.name for label in labels if not label.ignoreInEval}

######################### Config Options  #########################
mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]
input_size = (1080, 1920)

#Path to configuration file for HRNet. This file sets the hyper parameters of the model
config_path = 'hrnet/experiments/cityscapes/seg_hrnet_w18_small_v2_512x1024_sgd_lr1e-2_wd5e-4_bs_12_epoch484.yaml'

# Path to the saved model weights
model_file = 'hrnet/pretrained_models/hrnet_w18_small_v2_cityscapes_cls19_1024x2048_trainset.pth'

# Merge the model specific cofig file with the default conf file
cfg.merge_from_file(config_path)

# ADAPT input size
cfg.TEST.IMAGE_SIZE = [input_size[1], input_size[0]]
cfg.TEST.BASE_SIZE = input_size[1]
cfg.TEST.BATCH_SIZE_PER_GPU = 1
###################################################################


# Load the model graph
model = eval('models.'+cfg.MODEL.NAME +
                 '.get_seg_model')(cfg)

# Load the pretrained model weights
pretrained_dict = torch.load(model_file)
model_dict = model.state_dict()
pretrained_dict = {k[6:]: v for k, v in pretrained_dict.items()
                    if k[6:] in model_dict.keys()}
model_dict.update(pretrained_dict)
model.load_state_dict(model_dict)

# Transfer model to GPU
model = model.cuda()

# Read test image
raw_im = cv2.imread('cityscapes_sample.jpg')

# Preprocessing steps
raw_im = cv2.resize(raw_im, (input_size[1], input_size[0]))
im = raw_im / 255.0
im -= mean
im /= std
im = im.transpose([2,0,1])
im = np.float32(im)
model_in = torch.tensor(im).cuda().unsqueeze(0)

# Forward inference
out = model(model_in)

# Resize model predictions to input size 
preds = F.upsample(out, input_size, mode='bilinear')

# Copy preds back to cpu
preds = preds.detach().cpu().numpy()[0]

# Predictions have shape [num_classes=19, width, height] 
# Each pixel contains 19 probabilities corrosponding to p(class k)
# 
# We can simply take the argmax across the channels to reduce the tensor to a single
# label image.
preds = np.asarray(np.argmax(preds, axis=0), dtype=np.uint8)

save_img = Image.fromarray(preds)

# TODO map iamge to preds to known colorspace (car == red, person == blue, etc...)

save_img.save('demo_out.png')

