#!/bin/sh

###############################################################################
# Confuguration Block 
###############################################################################

# Path to data root (netharn must be configured
DATA_DIR="/mnt/sandbox/projects/noaa_adapt/data/"

# Path to training results directory (WARNING: Large files to be written)
TRAIN_DIR="/mnt/sandbox/models/noaa_adapt/"

# Path to model src dir
SRC_DIR="$DIR/../src/"

###############################################################################

# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


# Container information
image_name=gitlab.kitware.com:4567/adapt/adapt_dev:latest
container_name=adapt_dev
extra_docker_args=${2}

# Will be "true" is running, "false" if stopped, or "" if does not exist.
IS_RUNNING=$(docker inspect -f '{{.State.Running}}' ${container_name} 2>/dev/null)

if [ "$IS_RUNNING" = "true" ]; then
  echo "Container already running"
elif [ "$IS_RUNNING" = "false" ]; then
  # If the container exists but is stopped, start it.
  echo "Container is stopped, restarting it"
  docker start $container_name
else
  # Creating container.
  echo "Creating container"
  docker run --rm -dt \
    --network="host" \
    --gpus 0 \
    --ipc=host \
    -v "${DATA_DIR}:/data/" \
    -v "${TRAIN_DIR}:/train_dir/" \
    -v "${SRC_DIR}:/adapt_ws/" \
    ${extra_docker_args} \
    --name $container_name \
    $image_name \
    /bin/bash
fi

