import os
import cv2
#from PIL import Image
import numpy as np
import torch
from pathlib import Path
from torchvision import transforms
from imgaug.augmentables.segmaps import SegmentationMapsOnImage
from torch.utils.data import Dataset, DataLoader

class NOAAICE(Dataset):
    '''
    '''
    def __init__(self, datapath, trans_func=None, ignore_label=255, mode='train'):
        super(NOAAICE, self).__init__()
        assert mode in ('train', 'val', 'test')
        self.trans_func = trans_func
        self.ignore_label = ignore_label
       
        '''
        self.downsample_rates = { 1 : 7.212, 
                                  2 : 1.0, 
                                  3 : 2.999, 
                                  4 : 2.456, 
                                  5 : 2.260
                                  }
        '''

        self.downsample_rates = {
                                    1 : 11.96198, 
                                    2 : 1.0, 
                                    3 : 4.72323, 
                                    4 : 7.40502 , 
                                    5 : 3.80472 
                                    }

        self.label_map = {ii : ii - 1 for ii in range(1,6)}

        img_root = Path(datapath).joinpath('{}/images/'.format(mode))
        label_root = Path(datapath).joinpath('{}/labels/'.format(mode))

        self.img_paths = np.array(list(img_root.glob('*.png')))
        self.label_paths = np.array(list(label_root.glob('*.png')))

        # Shuffle samples
        indices = list(range(len(self.img_paths)))
        np.random.shuffle(indices)
        
        self.img_paths = self.img_paths[indices]
        self.label_paths = self.label_paths[indices]


        # TODO calculate for the dataset
        self.image_norm = transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
        self.len = len(self.img_paths)


    def __getitem__(self, idx):

        impth, lbpth = self.img_paths[idx], self.label_paths[idx]
        
        img, labels = cv2.imread(str(impth)), cv2.imread(str(lbpth), 0)

        # Balance labels according to downsample rates
        lab_flat = labels.flatten()
        for label, sample_rate in self.downsample_rates.items():
            lab_indices = np.where(lab_flat == label)[0]
            downsampled_indices = np.random.choice(lab_indices, 
                                            int(len(lab_indices) * ( 1 - (1.0 / sample_rate))), 
                                            replace=False)
            lab_flat[downsampled_indices] = 255
        labels = lab_flat.reshape(labels.shape)

        segmap = SegmentationMapsOnImage(labels, shape=labels.shape)
        image, labels = self.trans_func(image=img, segmentation_maps=segmap)

        image = torch.FloatTensor(image.transpose(2, 0, 1).astype(np.float32))
        labels = labels.get_arr().astype(np.int64)
       
        # Normalize
        image = image / 255.0
        image = self.image_norm(image)
        
        # Replace augmentation filler values (0) with ignore label
        labels[labels == 0] = self.ignore_label

        # Map labels to [0,k]
        for ll, new_ll in self.label_map.items():
            labels[labels == ll] = new_ll

        #print(np.unique(labels, return_counts=True))
        return image, labels

    def __len__(self):
        return self.len

