import sys
import gc
import os
import json

import sys
sys.path.append('/usr/lib/python3.6/dist-packages/')
from torch2trt import torch2trt

#sys.path.append('../src/')
import pytorch_lightning as pl
import glob
import psutil
import pandas as pd

from pathlib import Path
from PIL import Image
import time
import torch
import torchprof
import torch.autograd.profiler as profiler
import numpy as np
from models.BiSeNetV2.bisenetv2 import BiSeNetV2
from models.utils import align_and_update_state_dicts
from configs.default import cfg
from thop import profile, clever_format
#from train import LightningModel
from torchvision import transforms


input_scale = int(sys.argv[1])
use_trt = True

image_norm = transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))

baseline_mem = psutil.virtual_memory()
base_swap_mem = psutil.swap_memory()

#cfg.merge_from_file('/home/ddavila/nooa_adapt/segmentation_ws/src/configs/bisenet_noaaice.yaml')
#cfg.merge_from_file('/home/squadx/projects/noaa_adapt/segmentation_ws/src/configs/bisenet_noaaice.yaml')
cfg.merge_from_file('/home/squadx/segmentation_ws/src/configs/bisenet_noaaice.yaml')

base_size = 128

input_size = base_size * int(input_scale)

raw_im = np.ones((input_size, input_size, 3))
im = np.float32(raw_im).transpose([2,0,1])
im = image_norm(torch.tensor(im) / 255.0)

model = BiSeNetV2(cfg)
model_in = im.cuda().unsqueeze(0)
model = model.eval()
model = model.cuda()

if use_trt:
#    model = torch2trt(model, [model_in])
#    model_in = np.uint8(raw_im).transpose([2,0,1])
#    model_in = image_norm(torch.tensor(im) / 255.0)
#    model_in = im.cuda().unsqueeze(0)
    model = torch2trt(model, [model_in], fp16_mode=True)
    model_in = model_in.half()

times = []
with torch.no_grad():
    for i in range(20):
        start = time.time()
        out = model(model_in)[0]
        forward_mem = psutil.virtual_memory()
        forward_swap_mem = psutil.swap_memory()
        labels = np.argmax(out.cpu().numpy()[0], axis=0) + 1
        torch.cuda.synchronize()
        end = time.time()
        times.append(end - start)

    forward = 1/np.mean(times[1:])
    forward_ignore_first = 1/np.mean(times)

    if os.path.exists('results.json'):
        with open('results.json', 'r') as jf:
            results = json.load(jf)
    else:
        results = {}

    results[base_size * input_scale] = {
                'baseline_mem'  : baseline_mem._asdict(),
                'baseline_swap' : base_swap_mem._asdict(),
                'forward'       : forward_mem._asdict(),
                'forward_swap'  : forward_swap_mem._asdict(),
                'fps'           : forward,
                'fps_no_init'   : forward_ignore_first
                }

    with open('results.json', 'w') as jf:
        json.dump(results, jf)

